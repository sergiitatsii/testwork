import UIKit

enum AppAppearance {
  
  static func configure() {
    configureNavigationBar()
  }
  
  static func configureNavigationBar() {
    UINavigationBar.appearance().shadowImage = UIImage()
    UINavigationBar.appearance().setBackgroundImage(UIImage(), for: UIBarMetrics.default)
    UINavigationBar.appearance().backIndicatorImage = UIImage(named: "ic_back")
    UINavigationBar.appearance().backIndicatorTransitionMaskImage = UIImage(named: "ic_back")
    UINavigationBar.appearance().barStyle = .default
    UINavigationBar.appearance().tintColor = UIColor.white
    UINavigationBar.appearance().barTintColor = UIColor.white
    UINavigationBar.appearance().isTranslucent = true
    UINavigationBar.appearance().titleTextAttributes = [
      NSAttributedString.Key.foregroundColor : UIColor.white,
      NSAttributedString.Key.font : UIFont.Testwork.montserratRegular(16)]
  }
  
}

extension UINavigationController {
  override open var preferredStatusBarStyle: UIStatusBarStyle { return .lightContent }
}

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?

  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    AppAppearance.configure()
    IQKeyboardManager.shared.enable = true
    
    guard let vc = ControllersListViewController.instantiate() else { return false }
    let navVC = UINavigationController(rootViewController: vc)
    window?.rootViewController = navVC
    
    return true
  }

}

import UIKit

class ControllersListViewController: ConfigurableViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .navigationBarHidden
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
  }
  
  @IBAction func screen1ButtonPressed(_ sender: Any) {
    guard let signUpViewController = SignUpViewController.instantiate(storyboardName: "SignUpViewController", initial: false) else { return }
    navigationController?.pushViewController(signUpViewController, animated: true)
  }
  
  @IBAction func screen2ButtonPressed(_ sender: Any) {
    guard let newsfeedViewController = NewsfeedViewController.instantiate(initial: false) else { return }
    navigationController?.pushViewController(newsfeedViewController, animated: true)
  }
  
  @IBAction func screen3ButtonPressed(_ sender: Any) {
    guard let profileViewController = ProfileViewController.instantiate(initial: false) else { return }
    navigationController?.pushViewController(profileViewController, animated: true)
  }
  
  @IBAction func screen4ButtonPressed(_ sender: Any) {
    guard let timelineViewController = TimelineViewController.instantiate(initial: false) else { return }
    navigationController?.pushViewController(timelineViewController, animated: true)
  }
  
}

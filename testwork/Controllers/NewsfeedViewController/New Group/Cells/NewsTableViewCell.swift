import UIKit
import DTModelStorage

class NewsTableViewCell: UITableViewCell, ModelTransfer {
  
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var likesImageView: UIImageView!
  @IBOutlet weak var likesLabel: UILabel!
  @IBOutlet weak var commentsImageView: UIImageView!
  @IBOutlet weak var commentsLabel: UILabel!
  @IBOutlet weak var moreImageView: UIImageView!
  
  func update(with model: News) {
    pictureImageView.image = model.picture
    titleLabel.text = model.title
    likesImageView.image = model.likesImage
    likesLabel.text = "\(model.likesCount) Likes"
    commentsImageView.image = model.commentsImage
    commentsLabel.text = "\(model.commentsCount) Comments"
    moreImageView.image = UIImage(named: "ic_more")
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    pictureImageView.image = nil
    likesImageView.image = nil
    commentsImageView.image = nil
    titleLabel.text = nil
    likesLabel.text = nil
    commentsLabel.text = nil
    moreImageView.image = nil
  }
  
}


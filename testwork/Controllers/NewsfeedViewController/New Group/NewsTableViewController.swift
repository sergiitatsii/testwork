import UIKit
import DTTableViewManager
import DTModelStorage

class NewsTableViewController: UIViewController, DTTableViewManageable {
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    manager.register(NewsTableViewCell.self)
    manager.startManaging(withDelegate: self)
    
    manager.didSelect(NewsTableViewCell.self) { cell, model, indexPath in
      
    }
    
    manager.tableViewUpdater?.didUpdateContent = { _ in
      
    }
    manager.memoryStorage.setItems(fakeNews)
  }
  
}

import UIKit
import DTModelStorage

class NewsCollectionViewCell: UICollectionViewCell, ModelTransfer {
  
  @IBOutlet weak var pictureImageView: UIImageView!
  @IBOutlet weak var shadowImageView: UIImageView!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var likesImageView: UIImageView!
  @IBOutlet weak var likesLabel: UILabel!
  @IBOutlet weak var commentsImageView: UIImageView!
  @IBOutlet weak var commentsLabel: UILabel!
  
  func update(with model: News) {
    pictureImageView.image = model.picture
    shadowImageView.image = UIImage(named: "img_shadow")
    titleLabel.text = model.title
    likesImageView.image = model.likesImage
    likesLabel.text = "\(model.likesCount) Likes"
    commentsImageView.image = model.commentsImage
    commentsLabel.text = "\(model.commentsCount) Comments"
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    pictureImageView.image = nil
    shadowImageView.image = nil
    likesImageView.image = nil
    commentsImageView.image = nil
    titleLabel.text = nil
    likesLabel.text = nil
    commentsLabel.text = nil
  }
  
}

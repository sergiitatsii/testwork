import UIKit
import DTCollectionViewManager
import DTModelStorage

class NewsCollectionViewController: UIViewController, DTCollectionViewManageable {
  
  @IBOutlet weak var collectionView: UICollectionView?
  @IBOutlet weak var pageControl: UIPageControl!
  
  var news = [News]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    manager.register(NewsCollectionViewCell.self)
    manager.startManaging(withDelegate: self)
    manager.didSelect(NewsCollectionViewCell.self) { cell, model, indexPath in
      
    }
    
    manager.collectionViewUpdater?.didUpdateContent = { [weak self] _ in
      self?.pageControl.numberOfPages = self?.manager.memoryStorage.items(inSection: 0)?.count ?? 0
    }
    manager.memoryStorage.setItems(news)
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    guard let collectionView = collectionView else { return }
    let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
    let size = collectionView.frame.size
    guard size.height > 0 else { return }
    layout.itemSize = CGSize(width: size.width, height: size.height)
  }
  
}

// MARK: UIScrollViewDelegate
extension NewsCollectionViewController: UIScrollViewDelegate {
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    if let indexPath = collectionView?.indexPathsForVisibleItems.first {
      pageControl.currentPage = indexPath.row
    }
  }
}

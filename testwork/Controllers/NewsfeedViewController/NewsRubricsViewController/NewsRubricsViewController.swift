import UIKit
import LZViewPager

class NewsRubricsViewController: UIViewController, LZViewPagerDelegate, LZViewPagerDataSource {
  
  @IBOutlet weak var viewPager: LZViewPager!
  
  private var subControllers: [UIViewController] = []
  
  override func viewDidLoad() {
    super.viewDidLoad()
    viewPager.dataSource = self
    viewPager.delegate = self
    viewPager.hostController = self
    
    let allVC = UIViewController()
    allVC.title = "All"
    allVC.view.backgroundColor = UIColor.orange
    
    guard let featuredVC = NewsTableViewController.instantiateFromXib() else { return }
    featuredVC.title = "Featured"
    
    let popularVC = UIViewController()
    popularVC.title = "Popular"
    popularVC.view.backgroundColor = UIColor.green
    
    let myFavoritesVC = UIViewController()
    myFavoritesVC.title = "My Favorites"
    myFavoritesVC.view.backgroundColor = UIColor.blue
    
    subControllers = [allVC, featuredVC, popularVC, myFavoritesVC]
    viewPager.reload()
    viewPager.select(index: 1, animated: false)
  }
  
  // MARK: LZViewPagerDataSource
  func numberOfItems() -> Int {
    return subControllers.count
  }
  
  func controller(at index: Int) -> UIViewController {
    return subControllers[index]
  }
  
  func button(at index: Int) -> UIButton {
    let button = UIButton()
    button.titleLabel?.font = UIFont.Testwork.montserratRegular(14)
    button.setTitleColor(UIColor.Testwork.pink, for: .normal)
    button.setTitleColor(UIColor.white, for: .selected)
    return button
  }
  
  func buttonsAligment() -> ButtonsAlignment {
    return .center
  }
  
  func heightForHeader() -> CGFloat {
    return 52
  }
  
  func backgroundColorForHeader() -> UIColor {
    return UIColor.Testwork.red
  }
  
}

import UIKit

class NewsfeedViewController: ConfigurableViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .withHiddenShadowUnderNavigationBar
    title = "Newsfeed"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu"), style: .plain, target: self, action: #selector(menuButtonPressed))
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search"), style: .plain, target: self, action: #selector(searchButtonPressed))
  }
  
  @objc func menuButtonPressed() {
    navigationController?.popViewController(animated: true)
  }
  
  @objc func searchButtonPressed() {
    
  }
  
}

// MARK: Segues
extension NewsfeedViewController: SegueHandlerType {
  
  enum SegueIdentifier: String {
    case newsCollection = "NewsCollectionViewController"
    case newsRubrics = "NewsRubricsViewController"
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    switch segueIdentifier(for: segue) {
    case .newsCollection:
      guard let newsCollectionVC = segue.destination as? NewsCollectionViewController else { return }
      newsCollectionVC.news = fakeNews
    case .newsRubrics:
      print("NewsRubricsViewController segue is about to be performed.")
    }
  }
  
}

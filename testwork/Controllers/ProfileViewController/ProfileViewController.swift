import UIKit

class ProfileViewController: ConfigurableViewController {
  
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var shadowImageView: UIImageView!
  @IBOutlet weak var avatarImageView: UIImageView!
  @IBOutlet weak var facebookImageView: UIImageView!
  @IBOutlet weak var facebookFollowersLabel: UILabel!
  @IBOutlet weak var twitterImageView: UIImageView!
  @IBOutlet weak var twitterFollowersLabel: UILabel!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .withHiddenShadowUnderNavigationBar
    title = "Profile"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu"), style: .plain, target: self, action: #selector(menuButtonPressed))
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_edit"), style: .plain, target: self, action: #selector(editButtonPressed))
    
    backgroundImageView.image = UIImage(named: "bg_profile")
    shadowImageView.image = UIImage(named: "img_shadow")
    avatarImageView.image = UIImage(named: "image_avatar")
    facebookImageView.image = UIImage(named: "ic_facebook")
    facebookFollowersLabel.text = "786 Followers"
    twitterImageView.image = UIImage(named: "ic_twitter")
    twitterFollowersLabel.text = "456 Followers"
  }
  
  @objc func menuButtonPressed() {
    navigationController?.popViewController(animated: true)
  }
  
  @objc func editButtonPressed() {
    
  }
  
}


import UIKit

class SettingsTableViewController: UITableViewController {
  
  @IBOutlet weak var facebookSwitch: CustomSwitch!
  @IBOutlet weak var twitterSwitch: CustomSwitch!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    update(facebookSwitch)
    twitterSwitch.addTarget(self, action: #selector(twitterSwitchPressed), for: .touchUpInside)
    update(twitterSwitch)
  }

  @IBAction func facebookSwitchPressed(_ sender: Any) {
    update(facebookSwitch)
  }
  
  @objc func twitterSwitchPressed() {
    update(twitterSwitch)
  }
  
  private func update(_ customSwitch: CustomSwitch) {
    customSwitch.thumbTintColor = customSwitch.isOn ? UIColor.white : UIColor.Testwork.red
    customSwitch.layer.borderColor = customSwitch.isOn ? UIColor.clear.cgColor : UIColor.Testwork.lightGray.cgColor
  }
  
}

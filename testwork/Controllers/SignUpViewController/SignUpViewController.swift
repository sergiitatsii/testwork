import UIKit

class SignUpViewController: ConfigurableViewController {
  
  @IBOutlet weak var nextButton: UIButton!
  @IBOutlet weak var nextButtonHeight: NSLayoutConstraint!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .withHiddenShadowUnderNavigationBar
    
    if traitCollection.horizontalSizeClass == .regular && traitCollection.verticalSizeClass == .regular {
      nextButtonHeight.constant = 67.5
    }
    nextButton.layer.cornerRadius = nextButtonHeight.constant / 2
  }
  
  override func updateViewConstraints() {
    super.updateViewConstraints()
    
  }
  
}

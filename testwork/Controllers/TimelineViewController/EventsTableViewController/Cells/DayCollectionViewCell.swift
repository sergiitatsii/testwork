import UIKit

class DayCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var dayLabel: UILabel!
  @IBOutlet weak var monthLabel: UILabel!
  @IBOutlet weak var eventsCountLabel: UILabel!
  
  
  var model: Day? {
    didSet {
      updateUI()
    }
  }
  
  override var isSelected: Bool {
    didSet {
      if isSelected {
        
      } else {
        
      }
    }
  }
  
  func updateUI() {
    guard let day = model else { return }
    dayLabel.text = day.day
    monthLabel.text = day.month
    eventsCountLabel.text = day.eventsCount
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
  }
  
}

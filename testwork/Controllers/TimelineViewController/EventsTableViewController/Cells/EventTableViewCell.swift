import UIKit

class EventTableViewCell: UITableViewCell {
  
  @IBOutlet weak var statusImageView: UIImageView!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var ampmLabel: UILabel!
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subTitleLabel: UILabel!
  @IBOutlet weak var collectionView: UICollectionView!
  
  var participants = [UIImage]()
  
  var model: Event? {
    didSet {
      updateUI()
    }
  }
  
  func updateUI() {
    guard let event = model else { return }
    statusImageView.image = event.isDone ? UIImage(named: "ic_circle_green") : UIImage(named: "ic_circle_red")
    timeLabel.text = event.time
    ampmLabel.text = event.ampm
    titleLabel.text = event.title
    subTitleLabel.text = event.subTitle
    
    if event.participants.isEmpty {
      collectionView.isHidden = true
    }
    
    participants = event.participants
    collectionView.reloadData()
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.register(UINib.init(nibName: "ParticipantCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ParticipantCollectionViewCell")
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    statusImageView.image = nil
    timeLabel.text = nil
    ampmLabel.text = nil
    titleLabel.text = nil
    subTitleLabel.text = nil
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
  }
  
}

extension EventTableViewCell: UICollectionViewDataSource, UICollectionViewDelegate {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return participants.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParticipantCollectionViewCell", for: indexPath) as! ParticipantCollectionViewCell
    cell.model = participants[indexPath.row]
    return cell
  }
  
}

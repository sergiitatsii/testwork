import UIKit

class ParticipantCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var avatarImageView: UIImageView!
  
  var model: UIImage? {
    didSet {
      updateUI()
    }
  }
  
  func updateUI() {
    guard let image = model else { return }
    avatarImageView.image = image
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    avatarImageView.image = nil
  }
  
}

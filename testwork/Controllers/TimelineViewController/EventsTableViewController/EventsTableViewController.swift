import UIKit

class EventsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
  
  @IBOutlet weak var tableView: UITableView!
  
  var events = [Event]()
  
  override func viewDidLoad() {
    super.viewDidLoad()
    tableView.register(cellClass: EventTableViewCell.self)
    tableView.estimatedRowHeight = 125
    tableView.rowHeight = UITableView.automaticDimension
    
    events = fakeEvents
    tableView.reloadData()
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return events.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: EventTableViewCell.self.reuseID) as! EventTableViewCell
    let event = events[indexPath.row]
    cell.model = event
    return cell
  }
  
}

import UIKit
import DTTableViewManager
import DTModelStorage

class EventsTableViewController: UIViewController, DTTableViewManageable {
  
  @IBOutlet weak var tableView: UITableView!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    manager.register(TimelineTableViewCell.self)
    manager.startManaging(withDelegate: self)
    
    manager.didSelect(TimelineTableViewCell.self) { cell, model, indexPath in
      
    }
    
    manager.tableViewUpdater?.didUpdateContent = { _ in
      
    }
    manager.memoryStorage.setItems(fakeEvents)
  }
  
}

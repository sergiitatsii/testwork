import UIKit

class TimelineViewController: ConfigurableViewController {
  
  @IBOutlet weak var backgroundImageView: UIImageView!
  @IBOutlet weak var shadowImageView: UIImageView!
  @IBOutlet weak var collectionView: UICollectionView!
  
  let interitemSpacing: CGFloat = 15
  var days = [Day]()
  
  private var currentPage: Int = 0 {
    didSet {
      let indexPath = IndexPath(item: currentPage, section: 0)
      collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configuration = .withHiddenShadowUnderNavigationBar
    title = "Timeline"
    navigationItem.leftBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_menu"), style: .plain, target: self, action: #selector(menuButtonPressed))
    navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "ic_search"), style: .plain, target: self, action: #selector(searchButtonPressed))
    backgroundImageView.image = UIImage(named: "bg_timeline")
    shadowImageView.image = UIImage(named: "img_shadow")
    
    collectionView.register(UINib.init(nibName: "DayCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DayCollectionViewCell")
    days = fakeDays
    collectionView.performBatchUpdates(nil, completion: { [weak self] _ in
      self?.currentPage = 2
    })
    collectionView.reloadData()
  }
  
  @objc func menuButtonPressed() {
    navigationController?.popViewController(animated: true)
  }
  
  @objc func searchButtonPressed() {
    
  }
  
  // MARK: Private Functions
  private func setupLayout() {
    let layout = collectionView.collectionViewLayout as! UPCarouselFlowLayout
    let itemHeight = collectionView.bounds.height
    let itemSize = CGSize(width: itemHeight, height: itemHeight)
    layout.itemSize = itemSize
    layout.spacingMode = UPCarouselFlowLayoutSpacingMode.fixed(spacing: interitemSpacing)
    //layout.spacingMode = UPCarouselFlowLayoutSpacingMode.overlap(visibleOffset: 10)
  }
  
  private var pageSize: CGSize {
    let layout = collectionView.collectionViewLayout as! UPCarouselFlowLayout
    var pageSize = layout.itemSize
    if layout.scrollDirection == .horizontal {
      pageSize.width += layout.minimumLineSpacing
    } else {
      pageSize.height += layout.minimumLineSpacing
    }
    return pageSize
  }
  
}

// MARK: UICollectionViewDelegate & UICollectionViewDataSource
extension TimelineViewController: UICollectionViewDelegate, UICollectionViewDataSource {
  
  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return days.count
  }
  
  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DayCollectionViewCell", for: indexPath) as! DayCollectionViewCell
    cell.model = days[indexPath.row]
    return cell
  }
  
}

// MARK: UICollectionViewDelegateFlowLayout
extension TimelineViewController: UICollectionViewDelegateFlowLayout {
  
  func collectionView(_ collectionView: UICollectionView,
                      layout collectionViewLayout: UICollectionViewLayout,
                      sizeForItemAt indexPath: IndexPath) -> CGSize
  {
    setupLayout()
    let itemHeight = collectionView.bounds.height
    return CGSize(width: itemHeight, height: itemHeight)
  }
  
}

// MARK: UIScrollViewDelegate
extension TimelineViewController: UIScrollViewDelegate {
  
  func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
    let layout = collectionView.collectionViewLayout as! UPCarouselFlowLayout
    let pageSide = (layout.scrollDirection == .horizontal) ? pageSize.width : pageSize.height
    let offset = (layout.scrollDirection == .horizontal) ? scrollView.contentOffset.x : scrollView.contentOffset.y
    currentPage = Int(floor((offset - pageSide / 2) / pageSide) + 1)
  }
  
}

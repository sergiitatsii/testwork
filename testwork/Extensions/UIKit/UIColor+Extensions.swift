import UIKit

extension UIColor {
  
  enum Testwork {
    static var red: UIColor { return UIColor(hex: "#ff2d55") }
    static var pink: UIColor { return UIColor(hex: "#feb2c1") }
    static var gray: UIColor { return UIColor(hex: "#9BA3AD") }
    static var lightGray: UIColor { return UIColor(hex: "#cccccc") }
    static var darkGray: UIColor { return UIColor(red: 49, green: 49, blue: 49) }
  }
  
}

extension UIColor {
  
  convenience init(red: Int, green: Int, blue: Int) {
    let newRed = CGFloat(red)/255
    let newGreen = CGFloat(green)/255
    let newBlue = CGFloat(blue)/255
    
    self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
  }
  
  convenience init(hex: String, alpha: CGFloat = 1) {
    assert(hex[hex.startIndex] == "#", "Expected hex string of format #RRGGBB")
    
    let scanner = Scanner(string: hex)
    scanner.scanLocation = 1  // skip #
    
    var rgb: UInt32 = 0
    scanner.scanHexInt32(&rgb)
    
    self.init(
      red:   CGFloat((rgb & 0xFF0000) >> 16)/255.0,
      green: CGFloat((rgb &   0xFF00) >>  8)/255.0,
      blue:  CGFloat((rgb &     0xFF)      )/255.0,
      alpha: alpha)
  }
  
}

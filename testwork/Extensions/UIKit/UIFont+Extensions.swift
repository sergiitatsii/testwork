import UIKit

extension UIFont {
  
  enum Testwork {
    static func montserratRegular(_ size: CGFloat) -> UIFont {
      return UIFont(name: "Montserrat-Regular", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func montserratLight(_ size: CGFloat) -> UIFont {
      return UIFont(name: "Montserrat-Light", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func montserratSemiBold(_ size: CGFloat) -> UIFont {
      return UIFont(name: "Montserrat-SemiBold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
    
    static func montserratBold(_ size: CGFloat) -> UIFont {
      return UIFont(name: "Montserrat-Bold", size: size) ?? UIFont.systemFont(ofSize: size)
    }
  }
  
}

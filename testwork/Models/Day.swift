import Foundation
import UIKit

struct Day {
  var day: String
  var month: String
  var eventsCount: String
}

let fakeDays: [Day] = [
  Day(day: "21",
      month: "February",
      eventsCount: "7 Events"),
  Day(day: "22",
      month: "February",
      eventsCount: "9 Events"),
  Day(day: "23",
      month: "February",
      eventsCount: "15 Events"),
  Day(day: "24",
      month: "February",
      eventsCount: "9 Events"),
  Day(day: "25",
      month: "February",
      eventsCount: "11 Events")
]

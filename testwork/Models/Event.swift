import Foundation
import UIKit

struct Event {
  var isDone: Bool
  var time: String
  var ampm: String
  var title: String
  var subTitle: String
  var participants: [UIImage]
}

let fakeEvents: [Event] = [
  Event(isDone: true,
        time: "4:45",
        ampm: "PM",
        title: "Revice Wireframe",
        subTitle: "Company Work",
        participants: []),
  Event(isDone: true,
        time: "2:20",
        ampm: "PM",
        title: "Finish Home Screen",
        subTitle: "Home App",
        participants: [UIImage(named: "img_person1")!,
                       UIImage(named: "img_person2")!,
                       UIImage(named: "img_person3")!]),
  Event(isDone: false,
        time: "11:20",
        ampm: "AM",
        title: "External Work",
        subTitle: "Company Work",
        participants: []),
  Event(isDone: true,
        time: "9:40",
        ampm: "AM",
        title: "Create Workflow",
        subTitle: "Internal Project",
        participants: [UIImage(named: "img_person4")!,
                       UIImage(named: "img_person5")!])
]

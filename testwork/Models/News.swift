import Foundation
import UIKit

struct News {
  var picture: UIImage
  var title: String
  var likesImage: UIImage
  var likesCount: Int
  var commentsImage: UIImage
  var commentsCount: Int
}

let fakeNews: [News] = [
  News(picture: UIImage(named: "image_news_1")!,
       title: "Integer ut placerat purued non dignissim neque.",
       likesImage: UIImage(named: "ic_likes_red")!,
       likesCount: 15,
       commentsImage: UIImage(named: "ic_comments")!,
       commentsCount: 85),
  News(picture: UIImage(named: "image_news_2")!,
       title: "Morbi per tincidunt tellus sit of amet eros laoreet.",
       likesImage: UIImage(named: "ic_likes")!,
       likesCount: 26,
       commentsImage: UIImage(named: "ic_comments")!,
       commentsCount: 32),
  News(picture: UIImage(named: "image_news_3")!,
       title: "Fusce ornare cursus masspretium tortor integer placera.",
       likesImage: UIImage(named: "ic_likes_red")!,
       likesCount: 15,
       commentsImage: UIImage(named: "ic_comments")!,
       commentsCount: 21),
  News(picture: UIImage(named: "image_news_4")!,
       title: "Maecenas eu risus blanscelerisque massa non amcorpe.",
       likesImage: UIImage(named: "ic_likes")!,
       likesCount: 36,
       commentsImage: UIImage(named: "ic_comments")!,
       commentsCount: 15),
  News(picture: UIImage(named: "image_news_5")!,
       title: "Pellentesque non lorem diam. Proin at ex sollicia.",
       likesImage: UIImage(named: "ic_likes")!,
       likesCount: 11,
       commentsImage: UIImage(named: "ic_comments")!,
       commentsCount: 9)
]
